package pl.mateusz_szczukowski;

import java.util.List;

/**
 * Created by Mateusz on 11.11.2016.
 */
public interface AddressDao {
    public void clearAddress();
    public List<Address> getAllAddresses();
    public int addAddress (Address adderess);
}
