package pl.mateusz_szczukowski;

import java.util.List;
/**
 * Created by Mateusz on 11.11.2016.
 */
public interface ClientDetailsDao {
    public void clearClientDetails();
    public List<clientDetails> getAllClientDetails();
    public int addClientDetails (clientDetails clientDetails);
    
}
