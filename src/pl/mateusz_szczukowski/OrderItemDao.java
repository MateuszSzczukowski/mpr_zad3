package pl.mateusz_szczukowski;

import java.util.List;
/**
 * Created by Mateusz on 11.11.2016.
 */
public interface OrderItemDao {
    public void clearOrderItem();
    public List<orderItem> getAllOrderItems();
    public int addOrderItem (orderItem orderItem);
    
}
