package pl.mateusz_szczukowski;

import java.util.List;
/**
 * Created by Mateusz on 11.11.2016.
 */
public interface OrderDao {
    public void clearOrder();
    public List<Order> getAllOrders();
    public int addOrder (Order order);

}
